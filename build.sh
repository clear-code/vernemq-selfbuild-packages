#!/usr/bin/bash

set -ex

run() {
    "$@"
    result=$?
    if [ $result -ne 0 ]; then
	echo "Failed: $@ [$PWD]" >&2
	exit $result
    fi
    return 0
}

OUTPUT_DIR=../_buildroot
SOURCE_DIR=_build/default/rel
VERNEMQ_VERSION=2.0.1

build_filter () {
    run rm -fr $OUTPUT_DIR
    for d in var/log/vernemq/sasl var/lib/vernemq/data/broker var/lib/vernemq/data/msgsrore; do
	run mkdir -p $OUTPUT_DIR/$d
    done
    for d in usr/sbin usr/lib64 usr/share etc/vernemq; do
	run mkdir -p $OUTPUT_DIR/$d
    done

    for d in vernemq vmq-admin vmq-passwd; do
	run cp $SOURCE_DIR/vernemq/bin/$d $OUTPUT_DIR/usr/sbin/
    done
    for d in vernemq.conf vmq.acl; do
	run cp $SOURCE_DIR/vernemq/etc/$d $OUTPUT_DIR/etc/vernemq/
    done
    run cp -r $SOURCE_DIR/vernemq/share $OUTPUT_DIR/usr/share/vernemq
    run cp -r $SOURCE_DIR/vernemq $OUTPUT_DIR/usr/lib64/vernemq
    run rm -fr $OUTPUT_DIR/usr/lib64/vernemq/share
    run rm -fr $OUTPUT_DIR/usr/lib64/vernemq/etc
    run rm -fr $OUTPUT_DIR/usr/lib64/vernemq/log
}

build_rpm () {
    run fpm --force --output-type rpm --package vernemq-$VERNEMQ_VERSION-1.el9.x86_64.rpm
}

case $1 in
    rpm)
	build_rpm
	;;
    *)
	if [ ! -d vernemq ]; then
	    run git clone https://github.com/vernemq/vernemq.git
	fi
	cd vernemq
	run git checkout $VERNEMQ_VERSION
	rm -fr _build
	run cp -f ../vars/rpm_vars.config vars.config
	run make rel
	build_filter
	cd -
	build_rpm
	sha256sum vernemq-$VERNEMQ_VERSION-1.el9.x86_64.rpm >  vernemq-$VERNEMQ_VERSION-1.el9.x86_64.rpm.sha256sum
	;;
esac
