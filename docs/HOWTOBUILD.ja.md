# VerneMQをセルフビルドする方法

## 前提条件

* ビルド環境はRocky Linux 9.2
* ビルド対象のVerneMQのバージョンは2.0.1
* あらかじめビルドされた`esl-erlang_26.2.5_1~rockylinux~8.6_x86_64.rpm`を利用する

## 依存パッケージをインストールする

次のコマンドを実行してビルドに必要なパッケージをインストールします。

```bash
sudo dnf install -y ./esl-erlang_26.2.5_1~rockylinux~8.6_x86_64.rpm
sudo dnf install 'dnf-command(config-manager)'
sudo dnf config-manager --set-enabled devel
sudo dnf install -y make git gcc-c++ snappy-devel findutils which compat-openssl11
sudo dnf install -y ruby ruby-devel
sudo gem install fpm -v 1.15.1 --no-document
```

Ruby関連のパッケージはfpmを実行してRPMを生成するのに必要です。

## VerneMQのセルフビルド版をビルドする方法

Gitリポジトリをcloneしてmakeを実行します。

```bash
git clone https://gitlab.com/clear-code/vernemq-selfbuild-packages.git
cd vernemq-selfbuild-packages
make
```

しばらくすると、作業ディレクトリに次のRPMが生成されます。

* vernemq-2.0.1-1.el9.x86_64.rpm
* vernemq-2.0.1-1.el9.x86_64.rpm.sha256sum

## ビルドしたパッケージの動作確認方法

ビルドしたパッケージを次のようにしてインストールします:

```bash
sudo dnf install -y ./vernemq-2.0.1-1.el9.x86_64.rpm
```

次のコマンドを実行して、VerneMQを起動します:

```bash
sudo /usr/sbin/vernemq start
```

しばらくするとVerneMQが起動するので、次のコマンドを実行します:

```bash
curl http://localhost:8888/status
```

VerneMQのステータスページが表示されれば問題なく起動できています。
http://localhost:8888/status を任意のブラウザでアクセスしてもよいです。

![](vernemq-status.png)
