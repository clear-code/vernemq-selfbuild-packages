# How to build VerneMQ by yourself

## Prerequisite

* Build RPM package on Rocky Linux 9
* VerneMQ version is 2.0.1
* Use prebuilt `esl-erlang_26.2.5_1~rockylinux~8.6_x86_64.rpm`

## Install dependency packages

Execute the following commands to install dependency packages.

```bash
sudo dnf install -y ./esl-erlang_26.2.5_1~rockylinux~8.6_x86_64.rpm
sudo dnf install 'dnf-command(config-manager)'
sudo dnf config-manager --set-enabled devel
sudo dnf install -y make git gcc-c++ snappy-devel findutils which compat-openssl11
sudo dnf install -y ruby ruby-devel
sudo gem install fpm -v 1.15.1 --no-document
```

Ruby related packages are required for executing fpm to generate RPM.

## Build VerneMQ selfbuild edition

Execute the following commands to clone and build the package.

```bash
git clone https://gitlab.com/clear-code/vernemq-selfbuild-packages.git
cd vernemq-selfbuild-packages
make
```

After a while, RPM is available on current directly.

* vernemq-2.0.1-1.el9.x86_64.rpm
* vernemq-2.0.1-1.el9.x86_64.rpm.sha256sum

## How to test selfbuild VerneMQ

Install built binary:

```bash
sudo dnf install -y ./vernemq-2.0.1-1.el9.x86_64.rpm
```

Launch VerneMQ with the following command:

```
sudo /usr/sbin/vernemq start
```

After a while, you can check VerneMQ status with:

```bash
curl http://localhost:8888/status
```

You can browse VerneMQ status page.
or open http://localhost:8888/status with any web browser.

![](vernemq-status.png)
