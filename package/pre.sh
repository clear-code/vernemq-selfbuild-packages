#!/bin/sh

if ! getent passwd vernemq >/dev/null; then
    /usr/sbin/useradd --system --home-dir /var/lib/vernemq --shell /bin/bash vernemq
fi
