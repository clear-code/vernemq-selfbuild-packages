# VerneMQ selfbuild RPM packages

This repository provides the script to selfbuild [VerneMQ](https://vernemq.com/)
using https://github.com/vernemq/vernemq.

The target version of VerneMQ is 2.0.1.

Currently RPM package is available from https://vernemq.com/downloads/index.html,
but the downloadable version is v1.13.0 and it is not ready for production use
because of noted as "Free to test" and "a paid subscription is required for production use".

VerneMQ can be built from sources, but it can not be packaged such as deb or rpm
so it is not suitable for distribution.

This repository help to generate rpm package for production use.

# License

* Apache-2.0
