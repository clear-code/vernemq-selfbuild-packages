rpm:
	./build.sh

install:
	sudo rpm -ivh vernemq-*.rpm

uninstall:
	sudo dnf remove -y vernemq

purge:
	make uninstall
	sudo rm -fr /var/log/vernemq
	sudo rm -fr /var/lib/vernemq
	sudo rm -fr /etc/vernemq
	sudo userdel -r vernemq
